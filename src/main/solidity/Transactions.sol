pragma solidity ^0.6.12;

contract Transactions{
    
    struct Transaction{
        string from;
        string to;
        uint token;
    }
    
    event TransactionSuccessful ( 
        bool success,
        string from,
        string to,
        uint tokenValue
    );
    
    Transaction[] public transactions;
    
    string [] public transactionStrings;
    
    function createAndStoreTransaction(string memory from, string memory to, uint token) public{
        if(token < 1){
            emit TransactionSuccessful(false, from, to, token);
        }
        else{
            uint tokenValue = token;
            if(bytes(to).length > bytes(from).length){
                tokenValue = tokenValue -1;
            }
            Transactions.Transaction memory newTransaction = Transaction(from, to, tokenValue);
            transactions.push(newTransaction);
            emit TransactionSuccessful(true, from, to, tokenValue);
        }
    }
    
    function reshareFile(string memory oldFileHash, string memory userAddress, string memory newFileHash) public{
        for(uint i = 0; i < transactions.length; i++){
            if(keccak256(bytes(transactions[i].from)) == keccak256(bytes(oldFileHash)) && keccak256(bytes(transactions[i].to)) == keccak256(bytes(userAddress))){
                if(transactions[i].token > 0){
                    Transactions.Transaction memory newTransaction = Transaction(userAddress, newFileHash, transactions[i].token-1);
                    transactions.push(newTransaction);
                    emit TransactionSuccessful(true, userAddress, newFileHash, transactions[i].token-1);
                    return;
                }
                else{
                    emit TransactionSuccessful(false, userAddress, newFileHash, transactions[i].token);
                    return;
                }
                
            }
        }
    }
    
    function shareStoredFile(string memory from, string memory to) public {
        bool transactionSuccess = false;
        for(uint i = 0; i < transactions.length; i++){
            if(keccak256(bytes(transactions[i].to)) == keccak256(bytes(from))){
                Transactions.Transaction memory newTransaction = Transaction(from, to, transactions[i].token);
                transactions.push(newTransaction);
                transactionSuccess = true;
                emit TransactionSuccessful(true, from, to, transactions[i].token);
                return;
            }
            else{
                emit TransactionSuccessful(false, transactions[i].from, transactions[i].to, transactions[i].token);
            }
        }
        if(transactionSuccess ==  false){
            emit TransactionSuccessful(false, from, to, 0);
        }
    }
    
    function lookUpMatchingTransactions(string memory address1) public{
        for(uint i = 0; i < transactions.length; i++){
            if(keccak256(bytes(address1)) == keccak256(bytes(transactions[i].to))){
                transactionStrings.push(transactions[i].from);
            }
        }
    }
    
    function getTransactionsLength() private view returns (uint length){
        return (transactions.length);
    }
    
    function getTransactionStringsLength() private view returns (uint length){
        return (transactionStrings.length);
    }
    
    function pushChosenTransactionStrings(string memory address1) public{
        for(uint i = 0; i < transactions.length; i++){
            if((keccak256(bytes(transactions[i].from)) == (keccak256(bytes(address1))))){
                transactionStrings.push(transactions[i].from);
                transactionStrings.push(transactions[i].to);
            }
        }
    }
    
    function clearTransactionStrings () private {
        delete transactionStrings;
    }
}