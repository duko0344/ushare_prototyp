package UShare.SmartContract;

import io.reactivex.disposables.Disposable;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.EthFilter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

public class ListenerWrapperThread extends Thread{

    Transactions transactions;
    BigInteger token;
    boolean[] successArray;
    ArrayList<String> newFileHashes;
    CountDownLatch latch;
    String from;
    String to;
    String oldFileHash;
    Disposable transactionSuccessfulListener;
    boolean reShare;

    /**
     * The constructor of the listener wrapper thread.
     *
     * @param from          The sender of the transaction
     * @param to            The receiver of the transaction
     * @param oldFileHash   The file hash from a previous encryption of a file. Used to identify past transactions.
     * @param latch         The latch of the main Transaction thread
     * @param transactions  an instance of the smart contract
     * @param successArray  An array to tell the main thread if a transaction was successful or not
     * @param newFileHashes The hash of the current encrypted file
     * @param token         The amount of shares left for this file
     * @param reShare       A boolean to signal if the user wishes to either reshare or create a new transaction
     */
    public ListenerWrapperThread(String from, String to, String oldFileHash, CountDownLatch latch, Transactions transactions, boolean[] successArray, ArrayList<String> newFileHashes, BigInteger token, boolean reShare) {
        this.transactions = transactions;
        this.token = token;
        this.successArray = successArray;
        this.latch = latch;
        this.from = from;
        this.to = to;
        this.oldFileHash = oldFileHash;
        this.newFileHashes = newFileHashes;
        this.reShare = reShare;
    }

    @Override
    public void run() {
        if (!reShare) {
            try {
                executingTransactions();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else{
            try {
                checkForValidResharing();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * A method to contact the blockchain, create an initial transaction of new share and wait for the blockchain to answer.
     * If the transaction has been accepted, the listener will receive a receiver signalling the successful transaction.
     * It will then set the success array to true and unblock the main transaction thread.
     */
    public void executingTransactions() throws Exception {
        System.out.println("wrapper Thread");
        if (token.intValue() > 0) {
            try {
                transactions.createAndStoreTransaction(from, to, token).send();
                System.out.println("createAndStore has been executed: " + from + to + token);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            transactions.shareStoredFile(from, to).send();
            System.out.println("file is being shared: " + from + " " + to);
        }
        transactionSuccessfulListener = transactions.transactionSuccessfulEventFlowable(new EthFilter(DefaultBlockParameterName.EARLIEST, DefaultBlockParameterName.LATEST, transactions.getContractAddress().substring(2))).subscribe(transactionSuccessfulEventResponse ->
        {
            if (transactionSuccessfulEventResponse.from.equalsIgnoreCase(from) && transactionSuccessfulEventResponse.to.equalsIgnoreCase(to) && transactionSuccessfulEventResponse.success) {
                successArray[0] = true;
                System.out.println("sharing was successful");
                latch.countDown();
            } else {
                System.out.println("Failed to share file: " + transactionSuccessfulEventResponse.from +" "+ transactionSuccessfulEventResponse.to +" " + transactionSuccessfulEventResponse.success + " " +transactionSuccessfulEventResponse.tokenValue);
            }
        });
    }

    /**
     * A method to contact the blockchain, ask for a reshare of an existing transaction and wait for the blockchain to answer.
     * If the transaction has been accepted, the listener will receive a receiver signalling the successful transaction.
     * It will then set the success array to true and unblock the main transaction thread.
     */
    public void checkForValidResharing() throws Exception {
        System.out.println("Listener Wrapper: From: " + from + " To: " + to + " OldFileHash: " + oldFileHash);
        transactions.reshareFile(oldFileHash, from, to).send();
        transactionSuccessfulListener = transactions.transactionSuccessfulEventFlowable(new EthFilter(DefaultBlockParameterName.EARLIEST, DefaultBlockParameterName.LATEST, transactions.getContractAddress().substring(2))).subscribe(transactionSuccessfulEventResponse ->
        {
            if (from.equalsIgnoreCase(transactionSuccessfulEventResponse.from) && to.equalsIgnoreCase(transactionSuccessfulEventResponse.to) && transactionSuccessfulEventResponse.success) {
                successArray[0] = true;
                System.out.println("Resharing successful!");
                latch.countDown();
            } else if (from.equalsIgnoreCase(transactionSuccessfulEventResponse.from) && to.equalsIgnoreCase(transactionSuccessfulEventResponse.to)) {
                System.out.println("Resharing was not successful. Insufficent tokens." + transactionSuccessfulEventResponse.from + transactionSuccessfulEventResponse.to);
                successArray[0] = false;
                latch.countDown();
            }
            else{
                System.out.println("From: " + transactionSuccessfulEventResponse.from + " " + "To: " + transactionSuccessfulEventResponse.to + " " + "Success: " + transactionSuccessfulEventResponse.success);
            }
        });
    }


}
