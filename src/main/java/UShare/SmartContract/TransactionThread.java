package UShare.SmartContract;

import UShare.GUI.UShareGUI;
import UShare.SQL.SQLFunctions;

import java.io.IOException;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class TransactionThread extends Thread {

    /**
     * An instance of the UShareGUI this thread is connected to.
     */
    UShareGUI uShareGUI;

    /**
     * A CountDownLatch used to pause and continue this thread.
     */
    private CountDownLatch latch;

    /**
     * This array stores the information whether a transaction has been completed successfully.
     * It is an array to circumvent the restriction of overwriting variables in lambda arrays and only contains one boolean.
     */
    private boolean[] successArray;

    /**
     * An instance of the connection to the deployed smart contract. Gives access to all the functions of the smart contract.
     */
    private Transactions transactions;

    /**
     * An instance of the connection to the deployed smart contract. Gives access to all the functions of the smart contract.
     */
    private String ownAddress;

    /**
     * The addresses of the receivers of the shared file.
     */
    private ArrayList<String> receivers;

    /**
     * The hash of the encrypted shared file.
     */
    private String fileHash;

    /**
     * The old file hash of the same file. It stems from the previously encrypted data. Only relevant for re-sharing.
     */
    private String oldFileHash;

    /**
     * The amount of shares allowed for the file. Only relevant for the initial sharing process.
     */
    private BigInteger tokenValue;

    /**
     * The boolean responsible for distinguish between an initial share and a resharing process.
     */
    private boolean reShare;

    /**
     * A combination of the encrypted data + plus the file type that needs to be stored in the database.
     */
    private String dataToBeStored;

    public TransactionThread(UShareGUI uShareGUI, Transactions transactions, ArrayList<String> receivers, String dataToBeStored, String fileHash, String oldFileHash, int tokenValue, boolean reShare) {
        this.latch = new CountDownLatch(1);
        this.successArray = new boolean[1];
        this.transactions = transactions;
        this.tokenValue = BigInteger.valueOf(tokenValue);
        this.uShareGUI = uShareGUI;
        this.ownAddress = Transactions.getCredentialsFromPrivateKey().getAddress();
        this.receivers = receivers;
        this.dataToBeStored = dataToBeStored;
        this.fileHash = fileHash;
        this.oldFileHash = oldFileHash;
        this.reShare = reShare;
    }

    /**
     * Gets the latch of this thread.
     *
     * @return latch the CountDownLatch of this thread
     */
    public CountDownLatch getLatch() {
        return latch;
    }

    /**
     * Sets a new latch for this thread.
     *
     * @param latch the new CountDownLatch for this thread
     */
    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    /**
     * Runs the thread and therefor the executingTransactions-function
     */
    @Override
    public void run() {
        executingTransactions(reShare);
    }

    /**
     * A method to execute the commissioned transactions by using the ListenerWrapper thread to communicate with the blockchain.
     * After confirming the initial transaction, it goes into a for-loop to execute the transaction for every single receiver.
     * Once the transactions are completed, it writes the file hashes + the encrypted data to the database and adds
     * it to the ownSharedFiles list and corresponding .txt-file.
     *
     * @param reShare   boolean to tell whether to turn on the reShare-function inside of the ListenerWrapperThread
     */
    public void executingTransactions(boolean reShare){
        ListenerWrapperThread listenerWrapperThread = new ListenerWrapperThread(ownAddress, fileHash, oldFileHash, latch, transactions, successArray, null, tokenValue, reShare);
        listenerWrapperThread.start();
        try {
            getLatch().await(30000, TimeUnit.MILLISECONDS);
            System.out.println("first Transaction done..");
            setLatch(new CountDownLatch(1));
            if (successArray[0] == true) {
                for (int i = 0; i < receivers.size(); i++) {
                    if (successArray[0] == true) {
                        successArray[0] = false;
                        listenerWrapperThread = new ListenerWrapperThread(fileHash, receivers.get(i), oldFileHash, latch, transactions, successArray, null, BigInteger.valueOf(-1), false);
                        listenerWrapperThread.start();
                        getLatch().await(30000, TimeUnit.MILLISECONDS);
                        setLatch(new CountDownLatch(1));
                    }
                    else{
                        uShareGUI.displayWarningMessage("The sharing process could not be completed. The blockchain has rejected the transaction");
                        break;
                    }
                }
                if(successArray[0]){
                    uShareGUI.writeFileNameToListAndTxt(fileHash, "ownSharedFiles.txt");
                    SQLFunctions sqlFunctions = new SQLFunctions();
                    sqlFunctions.storeEncryptedData(oldFileHash, fileHash, dataToBeStored);
                    uShareGUI.displayWarningMessage("The file was shared successfully!");
                }
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
