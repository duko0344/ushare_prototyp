package UShare.SQL;

import java.sql.*;

public class SQLFunctions{

    static Connection conn;

    /**
     * The constructor establishing the connection to the MySQL-database.
     * It also creates the needed database if not already created and selects it.
     */
    public SQLFunctions() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String connectionUrl = "insert URL of the mySQL-database here";
        conn = DriverManager.getConnection(connectionUrl, "username", "password");
        PreparedStatement statement1 = conn.prepareStatement("CREATE DATABASE IF NOT EXISTS hash_table_database;");
        statement1.executeUpdate();
        PreparedStatement statement = conn.prepareStatement("USE hash_table_database");
        statement.executeUpdate();
    }

    /**
     * A method that stores data the the corresponding hashes in the database. If the previous hash is null, it
     * uses the currentHash for both entries.
     *
     * @param previousHash the file hash used to find the corresponding encrypted data
     * @param currentHash
     * @param data
     *
     * @return String the encrypted data that is retrieved from the database
     */
    public boolean storeEncryptedData (String previousHash, String currentHash, String data) throws SQLException {
        PreparedStatement statement1;
        if(previousHash == null){
            statement1 = conn.prepareStatement("INSERT INTO hash_table (previousHash, currentHash, data) VALUES (\""+ currentHash +"\",\""+ currentHash+"\",\"" + data +"\");");
        }
        else {
            statement1 = conn.prepareStatement("INSERT INTO hash_table (previousHash, currentHash, data) VALUES (\""+ previousHash +"\",\""+ currentHash+"\",\"" + data +"\");");
        }
        statement1.executeUpdate();
        return true;
    }
    
    /**
     * A method that queries the database for encrypted data that is correlated to the given file hash.
     *
     * @param fileHash the file hash used to find the corresponding encrypted data
     *
     * @return String the encrypted data that is retrieved from the database
     */
    public String fetchEncryptedData(String fileHash) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("select data from hash_table where currentHash=\""+ fileHash +"\"");
        ResultSet resultSet = statement.executeQuery();
        String result = "";
        if (resultSet.next()){
            return resultSet.getString("data");
        }
        else {
            return null;
        }
    }
}
