package UShare.GUI;

import UShare.Cryptography.RSAKeyPair;
import UShare.Cryptography.RSAKeyPairGenerator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class RegisterGUI extends JFrame{

    public RegisterGUI(){

        //GUI
        setTitle("UShare-PoC");
        setSize(300, 270);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JLabel l = new JLabel("New Username");
        JLabel l2 = new JLabel("New Password");
        JLabel l3 = new JLabel("Re-enter new Password");
        JLabel l4 = new JLabel("Enter private Ethereum key");

        JTextField username = new JTextField(20);
        JPasswordField password = new JPasswordField(20);
        JPasswordField checkPassword = new JPasswordField(20);
        JTextField privateEthKey = new JTextField(20);

        //button to register
        JButton registerButton = new JButton("register");
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!(password.getText().equals(checkPassword.getText()))){
                    JOptionPane.showMessageDialog(registerButton, "Passwords must match! Please try again");
                    return;
                }
                else {
                    File loginData = new File("loginData.txt");
                    File privateEthKeyFile = new File("privateEthKey.txt");
                    try {
                        if(loginData.createNewFile() && privateEthKeyFile.createNewFile()){
                            FileWriter myWriter = new FileWriter("loginData.txt");
                            myWriter.write(username.getText()+"\n");
                            myWriter.write(password.getText());
                            myWriter.close();
                            myWriter = new FileWriter("privateEthKey.txt");
                            myWriter.write(privateEthKey.getText());
                            myWriter.close();
                        }
                        else{
                            JOptionPane.showMessageDialog(registerButton, "You are already registered!");
                            EventQueue.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    LoginGUI m = null;
                                    try {
                                        m = new LoginGUI();

                                    } finally {
                                        m.setVisible(true);
                                        dispose();
                                    }
                                }
                            });
                        }
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                    final RSAKeyPairGenerator generator = new RSAKeyPairGenerator();
                    final RSAKeyPair keyPair = generator.generate();
                    File privateTxt = new File("privateKey.txt");
                    File publicTxt = new File("publicKey.txt");
                    File friends = new File("friends.txt");
                    File circles = new File("circles.txt");
                    File ownSharedFiles = new File("ownSharedFiles.txt");
                    File sharedFiles = new File("sharedFiles.txt");
                    try {
                        friends.createNewFile();
                        circles.createNewFile();
                        privateTxt.createNewFile();
                        publicTxt.createNewFile();
                        ownSharedFiles.createNewFile();
                        sharedFiles.createNewFile();
                        FileWriter myWriter = new FileWriter("privateKey.txt");
                        myWriter.write(keyPair.getBase64PrivateKey().replaceAll("\\s+",""));
                        myWriter.close();
                        myWriter = new FileWriter("publicKey.txt");
                        myWriter.write(keyPair.getBase64PublicKey().replaceAll("\\s+",""));
                        myWriter.close();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                    JOptionPane.showMessageDialog(registerButton, "Successfully registered!");
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            LoginGUI m = null;
                            try {
                                m = new LoginGUI();

                            } finally {
                                m.setVisible(true);
                                dispose();
                            }
                        }
                    });
                }
            }
        });

        //Layout
        Container pane = getContentPane();
        pane.setLayout(new FlowLayout());
        pane.add(l);
        pane.add(username);
        pane.add(l2);
        pane.add(password);
        pane.add(l3);
        pane.add(checkPassword);
        pane.add(l4);
        pane.add(privateEthKey);
        pane.add(registerButton);
    }

}
