package UShare.GUI;

import UShare.Cryptography.RSAKeyPair;
import UShare.Cryptography.RSAKeyPairGenerator;

import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.security.NoSuchAlgorithmException;

public class AddFriendGUI extends JFrame {

    public AddFriendGUI(){
        //GUI
        setTitle("Meine GUI");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JLabel l = new JLabel("Put in the name of the friend");
        JLabel l2 = new JLabel("Put in the address of the friend");
        JLabel l3 = new JLabel("Put in the public key of the friend");

        JTextField username = new JTextField(20);
        JTextField address = new JTextField(20);
        JTextField publicKey = new JTextField(20);

        //Button to check the credentials and log into the main GUI
        JButton addFriendButton = new JButton("Add Friend");
        addFriendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!username.getText().equals("") && !publicKey.getText().equals("")){
                    try {
                        FileWriter myWriter = new FileWriter("friends.txt", true);
                        myWriter.write(username.getText() + " " + address.getText().toLowerCase() + " " + publicKey.getText()+"\n");
                        myWriter.close();
                        UShareGUI.updateAllListsFromTxt();
                        dispose();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
                else {
                    JOptionPane.showMessageDialog(null, "Neither Username nor public key can be left empty", "Warning", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        //button to register
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JButton generatePublicKeyButton = new JButton("Generate key");
        generatePublicKeyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final RSAKeyPairGenerator generator = new RSAKeyPairGenerator();
                final RSAKeyPair keyPair = generator.generate();
                publicKey.setText(keyPair.getBase64PrivateKey().replaceAll("\\s+",""));
            }
        });

        //Layout
        Container pane = getContentPane();
        GridBagLayout gbl = new GridBagLayout();
        pane.setLayout(gbl);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5,5,5,5);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(l, gbc);
        pane.add(l);

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridwidth = 3;
        gbc.gridheight = 1;
        gbl.setConstraints(username, gbc);
        pane.add(username);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(l2, gbc);
        pane.add(l2);

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 3;
        gbc.gridheight = 1;
        gbl.setConstraints(address, gbc);
        pane.add(address);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(l3, gbc);
        pane.add(l3);

        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridwidth = 3;
        gbc.gridheight = 1;
        gbl.setConstraints(publicKey, gbc);
        pane.add(publicKey);

        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(addFriendButton, gbc);
        pane.add(addFriendButton);

        gbc.gridx = 2;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(cancelButton, gbc);
        pane.add(cancelButton);

        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(generatePublicKeyButton, gbc);
        pane.add(generatePublicKeyButton);
        pack();
        /*pane.add(l);
        pane.add(username);
        pane.add(l2);
        pane.add(password);
        pane.add(loginButton);
        pane.add(registerButton);*/
    }
}
