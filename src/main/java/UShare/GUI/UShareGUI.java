package UShare.GUI;

import UShare.Cryptography.AsymmetricCryptography;
import UShare.Cryptography.CryptoException;
import UShare.SQL.SQLFunctions;
import UShare.SmartContract.TransactionThread;
import UShare.SmartContract.Transactions;
import org.apache.commons.io.FileUtils;

import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class UShareGUI extends JFrame {

    // Jlabel to show the files user selects
    private static JLabel l;

    private static List friendsList;

    private static List circlesList;

    private static List sharedFilesList;

    private static List ownSharedFilesList;

    private String decryptedFileLocation;

    private AsymmetricCryptography asymmetricCryptography;

    private Transactions transactions;

    private SQLFunctions sqlFunctions;

    public static String privateKey;

    /**
     * Constructor for the main UShare-GUI
     */
    public UShareGUI() throws NoSuchPaddingException, NoSuchAlgorithmException, IOException, SQLException, ClassNotFoundException {

        /**
         * an instance of the cryptographic method used
         */
        asymmetricCryptography = new AsymmetricCryptography();

        /**
         * the private ethereum key of the user that is read at startup
         */
        privateKey = FileUtils.readFileToString(new File("privateEthKey.txt"), Charset.defaultCharset());

        /**
         * the connection to the blockchain is being established and the object of this connection is stored as "transactions"
         */
        transactions = Transactions.establishConnection();

        /**
         * the connection to the database is being established and the object of this connection is stored as "sqlFunctions"
         */
        sqlFunctions = new SQLFunctions();

        /**
         * GUI - General instruction
         */
        setTitle("UShare-PoC");
        setSize(300, 400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        /**
         * The labels for the GUI
         */
        l = new JLabel("No file selected");
        JLabel friendsLabel = new JLabel("Friends");
        JLabel circlesLabel = new JLabel("Circles");
        JLabel ownSharedFilesLabel = new JLabel("My own shared files");
        JLabel filesSharedWithMeLabel = new JLabel("Files shared with me");

        /**
         * Creates the button that is used to reshare a file
         */
        JButton reshareButton = new JButton("Reshare a file");
        reshareButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (sharedFilesList.getSelectedItem() != null && (friendsList.getSelectedItem() != null || circlesList.getSelectedItem() != null)) {
                    String fileName = downloadFile();
                    System.out.println(fileName + "the file name");
                    File selectedFile = new File(fileName);
                    String path = selectedFile.getAbsolutePath();
                    String[] split = fileName.split("\\.");
                    String dataType = "." + split[1];
                    String filePath = new File("").getAbsolutePath();
                    String oldFileHash = "";
                    String stringSaver;
                    try{
                        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath + "/sharedFiles.txt"));
                        while (!((stringSaver = bufferedReader.readLine()) == null)) {
                            String[] splitString = stringSaver.trim().split("\\s+");
                            if(splitString[0].equals(sharedFilesList.getSelectedItem())) {
                                oldFileHash = splitString[1];
                                break;
                            }
                        }
                    }catch (IOException ioException) {
                        ioException.printStackTrace();
                    }

                    executeSharingProcess(BigInteger.valueOf(-1), dataType, path, oldFileHash, true);
                } else {
                    JOptionPane.showMessageDialog(reshareButton, "Choose a friend or circle AND the file you want to reshare!", "Warning", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        /**
         * Creates the button to add a friend
         */
        JButton addFriendsButton = new JButton("Add a friend");
        addFriendsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddFriendGUI m = null;
                try {
                    m = new AddFriendGUI();

                } finally {
                    m.setVisible(true);
                }
            }
        });

        /**
         * Creates the button to add a circle
         */
        JButton addCircleButton = new JButton("Add a circle");
        addCircleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddCircleGUI m = null;
                try {
                    List transferableFriendsList = new List();
                    for(int i = 0; i < friendsList.getItemCount(); i++){
                        transferableFriendsList.add(friendsList.getItem(i));
                    }
                    m = new AddCircleGUI(transferableFriendsList);

                } finally {
                    m.setVisible(true);
                }
            }
        });

        /**
         * Creates the button to update the shared files list
         */
        JButton updateSharedFilesButton = new JButton("Update shared files");
        updateSharedFilesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    lookUpNewSharedFiles();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });

        /**
         * Creates the button to delete a friend from the list
         */
        JButton deleteFriendButton = new JButton("Delete Friend");
        deleteFriendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(friendsList.getSelectedItem() != null){
                    String friendToDelete = friendsList.getSelectedItem();
                    friendsList.remove(friendToDelete);
                    File friendsTxt = new File("friends.txt");
                    try {
                        ArrayList<String> lines = (ArrayList<String>) FileUtils.readLines(friendsTxt, StandardCharsets.UTF_8);
                        ArrayList<String> updatedLines = (ArrayList<String>) lines.stream().filter(s -> !s.contains(friendToDelete)).collect(Collectors.toList());
                        FileUtils.writeLines(friendsTxt, updatedLines, false);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }

                }
                else {
                    JOptionPane.showMessageDialog(deleteFriendButton, "Choose a friend from the list to remove him/her!", "Warning", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        /**
         * Creates the button to download a file from the shared files list
         */
        JButton dowloadFileButton = new JButton("Download file");
        dowloadFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(sharedFilesList.getSelectedItem() != null) {
                    String fileName = downloadFile();
                    JOptionPane.showMessageDialog(dowloadFileButton, "The file " + fileName + " was successfully downloaded!", "Warning", JOptionPane.WARNING_MESSAGE);
                }
                else{
                    JOptionPane.showMessageDialog(dowloadFileButton, "Choose a file from the shared files list", "Warning", JOptionPane.WARNING_MESSAGE);

                }
            }
        });

        /**
         * Creates the button to delete a circle from the list
         */
        JButton deleteCircleButton = new JButton("Delete Circle");
        deleteCircleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    if(circlesList.getSelectedItem() != null){
                        String circleToDelete = circlesList.getSelectedItem();
                        circlesList.remove(circleToDelete);
                        File circlesTxt = new File("circles.txt");
                        try {
                            ArrayList<String> lines = (ArrayList<String>) FileUtils.readLines(circlesTxt, StandardCharsets.UTF_8);
                            ArrayList<String> updatedLines = (ArrayList<String>) lines.stream().filter(s -> !s.contains(circleToDelete)).collect(Collectors.toList());
                            FileUtils.writeLines(circlesTxt, updatedLines, false);
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }

                    }
                    else {
                        JOptionPane.showMessageDialog(deleteCircleButton, "Choose a circle from the list to remove it!", "Warning", JOptionPane.WARNING_MESSAGE);
                    }
                }
        });

        /**
         * Creates the button to share a local file
         */
        JButton shareButton = new JButton("Share a local file");
        shareButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(friendsList.getSelectedItem() != null || circlesList.getSelectedItem() != null){
                    JFileChooser j = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
                    int r;
                    r = j.showSaveDialog(null);
                    if (r == JFileChooser.APPROVE_OPTION) {
                        String path = j.getSelectedFile().getAbsolutePath();
                        String[] split = path.split("\\.");
                        String dataType = "." + split[split.length-1];
                        String answer = JOptionPane.showInputDialog(shareButton, "How many times do you want to allow the receiver/receivers to reshare your file?");
                        if(answer != null && answer.matches("\\d+") && Integer.parseInt(answer) > 0){
                            BigInteger tokenValue = BigInteger.valueOf(Integer.parseInt(answer));
                            executeSharingProcess(tokenValue, dataType, path, null, false);
                        }
                        else {
                            JOptionPane.showMessageDialog(shareButton, "Token value must be a number greater than zero", "Warning", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                }
                else{
                    JOptionPane.showMessageDialog(shareButton, "You need to select either an element from the friends list or the circles list!");
                }
            }
        });


        /**
         * The displayed lists are being initialized an updated with their respective .txt-files
         */
        sharedFilesList = new List();
        friendsList = new List();
        circlesList = new List();
        ownSharedFilesList = new List();
        UShareGUI.updateAllListsFromTxt();

        /**
         * mouse listeners are added to make sure only one file/contact can be selected at any given time
         */
        circlesList.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                if(e.getClickCount()==1){
                    for (int i = 0; i < friendsList.getItemCount(); i++) {
                        friendsList.deselect(i);
                    }
                }
            }
        });

        friendsList.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                if(e.getClickCount()==1){
                    for (int i = 0; i < circlesList.getItemCount(); i++) {
                        circlesList.deselect(i);
                    }
                }
            }
        });

        ownSharedFilesList.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                if(e.getClickCount()==1){
                    for (int i = 0; i < sharedFilesList.getItemCount(); i++) {
                        sharedFilesList.deselect(i);
                    }
                }
                else if(e.getClickCount() ==  2){
                    renameFile(ownSharedFilesList, "ownSharedFiles.txt");
                }
            }
        });

        sharedFilesList.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                if(e.getClickCount()==1){
                    for (int i = 0; i < ownSharedFilesList.getItemCount(); i++) {
                        ownSharedFilesList.deselect(i);
                    }
                }
                else if(e.getClickCount() ==  2){
                    renameFile(sharedFilesList, "sharedFiles.txt");
                }
            }
        });

        /**
         * the GridBagLayout is being initialized and subsequently filled
         */
        Container pane = getContentPane();
        GridBagLayout gbl = new GridBagLayout();
        pane.setLayout(gbl);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5,5,5,5);

        addComponentToPane(pane, gbl, gbc, shareButton, 0, 0, 1 ,1);
        addComponentToPane(pane, gbl, gbc, reshareButton, 0, 1, 1 ,1);
        addComponentToPane(pane, gbl, gbc, ownSharedFilesLabel, 1, 0, 1 ,1);
        addComponentToPane(pane, gbl, gbc, filesSharedWithMeLabel, 2, 0, 1 ,1);
        addComponentToPane(pane, gbl, gbc, ownSharedFilesList, 1, 1, 1 ,2);
        addComponentToPane(pane, gbl, gbc, sharedFilesList, 2, 1, 1 ,2);
        addComponentToPane(pane, gbl, gbc, updateSharedFilesButton, 0, 3, 1 ,2);
        addComponentToPane(pane, gbl, gbc, dowloadFileButton, 0, 2, 1 ,1);
        addComponentToPane(pane, gbl, gbc, friendsLabel, 1, 3, 1 ,1);
        addComponentToPane(pane, gbl, gbc, circlesLabel, 2, 3, 1 ,1);
        addComponentToPane(pane, gbl, gbc, friendsList, 1, 4, 1 ,2);
        addComponentToPane(pane, gbl, gbc, circlesList, 2, 4, 1 ,2);
        addComponentToPane(pane, gbl, gbc, addFriendsButton, 1, 6, 1 ,1);
        addComponentToPane(pane, gbl, gbc, addCircleButton, 2, 6, 1 ,1);
        addComponentToPane(pane, gbl, gbc, deleteFriendButton, 1, 7, 1 ,1);
        addComponentToPane(pane, gbl, gbc, deleteCircleButton, 2, 7, 1 ,1);

        pack();
        //setting the default file location for decryption
        decryptedFileLocation = new File("").getAbsolutePath();
    }
    /**
     * A method that updates all the displayed lists in the main GUI with all the information available in the
     * corresponding .txt-documents.
     */
    public static void updateAllListsFromTxt() throws IOException {
        if(friendsList != null && circlesList != null && ownSharedFilesList != null && sharedFilesList != null) {
            updateList("friends.txt", friendsList);
            updateList("circles.txt", circlesList);
            updateList("ownSharedFiles.txt", ownSharedFilesList);
            updateList("sharedFiles.txt", sharedFilesList);
        }
    }

    /**
     * A method that adds all entries from a .txt-file to a list
     *
     * @param fileName  the name of the file that the new entries are taken out of
     * @param list      the list that is being updated
     */
    public static void updateList(String fileName, List list) throws IOException {
        list.removeAll();
        String stringSaver = "";
        String filePath = new File("").getAbsolutePath();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath + "/" + fileName));
        while ((stringSaver = bufferedReader.readLine()) != null) {
            String[] splitString = stringSaver.trim().split("\\s+");
            list.add(splitString[0]);
            if (stringSaver.isEmpty()) {
                break;
            }
        }
    }

    /**
     * A method to rename a file both inside the main GUI and inside the corresponding .txt file.
     * A file name cannot contain spaces.
     *
     * @param list the list that currently contains the old file name
     * @param path the path of the corresponding .txt file
     */
    private void renameFile(List list, String path){
        if(list.getSelectedItem() != null){
            String newFileName = JOptionPane.showInputDialog(list, "Enter the new name for your file!");
            if(!newFileName.contains(" ")){
                String fileToRename = list.getSelectedItem();
                list.remove(fileToRename);
                File sharedFilesTxt = new File(path);
                try {
                    ArrayList<String> lines = (ArrayList<String>) FileUtils.readLines(sharedFilesTxt, StandardCharsets.UTF_8);
                    System.out.println(lines.get(0));
                    ArrayList<String> updatedLines = (ArrayList<String>) lines.stream().filter(s -> !s.contains(fileToRename + " ")).collect(Collectors.toList());
                    ArrayList<String> lineToChange = (ArrayList<String>) lines.stream().filter(s -> s.contains(fileToRename + " ")).collect(Collectors.toList());
                    String[] nameAndHash = lineToChange.get(0).split("\\s+");
                    updatedLines.add(newFileName + " " + nameAndHash[1]);
                    FileUtils.writeLines(sharedFilesTxt, updatedLines, false);
                    updateAllListsFromTxt();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
            else{
                displayWarningMessage("The file name cannot contain spaces!");
            }

        }
    }

    /**
     * A method to add a file name to the ownSharedFiles.txt and the own shared files list. Using a file hash for
     * this method is strongly recommended as this is used to identify the file itself on the database.
     * The file name can be manually changed by double clicking the file hash in the main GUI
     *
     * @param fileHash the hash that is being written into the ownSharedFiles.txt and the corresponding list
     */
    public void writeFileNameToListAndTxt(String fileHash, String path) throws IOException {
        FileWriter myWriter = new FileWriter(path, true);
        myWriter.write(fileHash + " " + fileHash + "\n");
        myWriter.close();
        updateAllListsFromTxt();
    }

    /**
     * A method to add a file name to the ownSharedFiles.txt and the own shared files list. Using a file hash for
     * this method is strongly recommended as this is used to identify the file itself on the database.
     * The file name can be manually changed by double clicking the file hash in the main GUI
     *
     * @param fileHash the hash that is being written into the ownSharedFiles.txt and the corresponding list
     */
    public void addSharedFile(String fileHash) throws IOException {
        String filePath = new File("").getAbsolutePath();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath + "/sharedFiles.txt"));
        boolean newEntry = true;
        String stringSaver = "";
        while ((stringSaver = bufferedReader.readLine()) != null) {
            String[] splitString = stringSaver.trim().split("\\s+");
            if (splitString[1].equals(fileHash)) {
                newEntry = false;
                break;
            }
            if (stringSaver.isEmpty()) {
                break;
            }
        }
        if (newEntry) {
            writeFileNameToListAndTxt(fileHash, "sharedFiles.txt");
        }
    }

    /**
     * A method to start the encryption and the sharing process of the selected file. The started thread will notify the user if the
     * the sharing process has been completed successfully and then write the encrypted file to the DB.
     *
     * @param path                  the path of the shared file
     * @param tokenValue            the amount of shares possible with this file
     * @param addresses             the addresses of the receivers of the sharing process
     * @param dataType              the data type of the shared file
     * @param reShare               a boolean that states whether this transaction is an initial share (false) or a reShare (true)
     */
    public void encryptAndShareAndStoreFile(String path, BigInteger tokenValue, ArrayList<String> addresses, String circleName, String publicKey, String oldFileHash, String dataType, boolean reShare) throws SQLException, ClassNotFoundException, IOException, CryptoException {
        String encryptedData = asymmetricCryptography.encryptFile(path, publicKey);
        String dataToBeStored;
        if(circleName != null){
            dataToBeStored = encryptedData + "." + circleName + dataType;
        }
        else{
            dataToBeStored = encryptedData + dataType;
        }
        TransactionThread transactionThread = new TransactionThread(this, transactions, addresses, dataToBeStored, String.valueOf(encryptedData.hashCode()), oldFileHash, tokenValue.intValue(), reShare);
        transactionThread.start();
    }

    /**
     * A method to check the blockchain for newly shared files that are affiliated with the owner´s address
     */
    public void lookUpNewSharedFiles() throws Exception {
        transactions.lookUpNewSharedFiles(this);
    }

    /**
     * A method to display a warning message for the user
     *
     * @param message the message that is being presented inside of the popup window
     */
    public void displayWarningMessage(String message) {
        JOptionPane.showMessageDialog(null, message, "Warning", JOptionPane.WARNING_MESSAGE);
    }

    /**
     * A method to download the selected file from the connected DB, decrypt it and store it inside of the program folder
     */
    public String downloadFile() {
        try {
            String privateKey = FileUtils.readFileToString(new File("privateKey.txt"), StandardCharsets.UTF_8);
            System.out.println(privateKey);
            String encryptedData = sqlFunctions.fetchEncryptedData(sharedFilesList.getSelectedItem());
            System.out.println(encryptedData);
            String[] split = encryptedData.split("\\.");
            String encryptedDataStripped = split[0];
            String dataType = "";
            String circleNameHash = "";
            if(split.length == 2){
                dataType = "." + split[1];
            }
            else if(split.length == 3){
                circleNameHash = split[1];
                dataType = "." + split[2];
                String filePath = new File("").getAbsolutePath();
                BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath + "/circles.txt"));
                String stringSaver = "";
                while ((stringSaver = bufferedReader.readLine()) != null) {
                    String[] splitString = stringSaver.trim().split("\\s+");
                    if (String.valueOf(splitString[0].hashCode()).equals(circleNameHash)) {
                        privateKey = splitString[splitString.length-2]; //the location of the private key of the circle
                        break;
                    }
                }
            }
            System.out.println(encryptedData);
            return asymmetricCryptography.decryptFile(encryptedDataStripped, privateKey, dataType, sharedFilesList.getSelectedItem());
        } catch (SQLException | IOException | CryptoException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * A method to execute the sharing process of a given file with given receivers.
     * It reads the related files gather all the addresses of the receivers and hands them over to the transaction thread.
     * It starts an independent thread to outsource the blockchain communication.
     *
     * @param tokenValue the amount of shares possible for this file
     * @param dataType   the type of data that is being shared. Examples are .txt or .jpg
     * @param path
     */
    public void executeSharingProcess(BigInteger tokenValue, String dataType, String path, String oldFileHash, boolean reShare) {
        try {
            ArrayList<String> recipientNames = new ArrayList();
            String publicKey = "";
            String circleNameHash = null;
            if (friendsList.getSelectedItem() != null) {
                recipientNames.add(friendsList.getSelectedItem());
            } else if (circlesList.getSelectedItem() != null) {
                String filePath = new File("").getAbsolutePath();
                BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath + "/circles.txt"));
                String stringSaver;
                circleNameHash = String.valueOf(circlesList.getSelectedItem().hashCode());
                while ((stringSaver = bufferedReader.readLine()) != null) {
                    String[] splitString = stringSaver.trim().split("\\s+");
                    if (String.valueOf(splitString[0].hashCode()).equals(circleNameHash)) {
                        for (int i = 1; i < splitString.length - 2; i++) {
                            System.out.println(splitString[i]);
                            recipientNames.add(splitString[i]);
                        }
                        publicKey = splitString[splitString.length - 1];
                        break;
                    }
                }
            }
            String filePath = new File("").getAbsolutePath();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath + "/friends.txt"));
            String stringSaver;
            int exchangeCounter = 0;
            while ((stringSaver = bufferedReader.readLine()) != null) {
                String[] splitString = stringSaver.trim().split("\\s+");
                for (int i = 0; i < recipientNames.size(); i++) {
                    if (recipientNames.get(i).equals(splitString[0])) {
                        recipientNames.set(i, splitString[1]);
                        exchangeCounter++;
                    }
                    if (exchangeCounter == recipientNames.size()) {
                        if (publicKey.equals("")) {
                            publicKey = splitString[2];
                        }
                        break;
                    }
                }
            }
            encryptAndShareAndStoreFile(path, tokenValue, recipientNames, circleNameHash, publicKey, oldFileHash, dataType, reShare);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (CryptoException cryptoException) {
            cryptoException.printStackTrace();
        }
    }

    /**
     * A method to add a specific component to the pane.
     *
     * @param pane      the pane that contains every element of the GUI
     * @param gbl       the type of layout that the GUI is constructed with
     * @param gbc       the layout constraints
     * @param component the component that is supposed to be added
     * @param gridx     the x coordinate of the component
     * @param gridy     the y coordinate of the component
     * @param width     the width of the component
     * @param height    the height of the component
     */
    public static void addComponentToPane(Container pane, GridBagLayout gbl, GridBagConstraints gbc, Component component, int gridx, int gridy, int width, int height){
        gbc.gridx = gridx;
        gbc.gridy = gridy;
        gbc.gridwidth = width;
        gbc.gridheight = height;
        gbl.setConstraints(component, gbc);
        pane.add(component);
    }
}
