package UShare.GUI;

import UShare.Cryptography.CryptoException;

import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public class LoginGUI extends JFrame{

    public LoginGUI(){
        //GUI
        setTitle("UShare-PoC");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JLabel usernameLabel = new JLabel("Username");
        JLabel passwordLabel = new JLabel("Password");

        JTextField usernameField = new JTextField(20);
        JPasswordField passwordField = new JPasswordField(20);

        //Button to check the credentials and log into the main GUI
        JButton loginButton = new JButton("log in");
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String filePath = new File("").getAbsolutePath();
                    File credentials = new File(filePath + "/loginData.txt");
                    if(!credentials.exists()){
                        JOptionPane.showMessageDialog(loginButton, "You are not registered!");
                        return;
                    }
                    FileReader fileReader = new FileReader(credentials);
                    BufferedReader bufferedReader = new BufferedReader(fileReader);
                    String usernameInput = bufferedReader.readLine();
                    String passwordInput = bufferedReader.readLine();

                    if(passwordInput.equals(passwordField.getText()) && usernameInput.equals(usernameField.getText())){
                        EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                UShareGUI m = null;
                                try {
                                    m = new UShareGUI();
                                } catch (NoSuchPaddingException e) {
                                    e.printStackTrace();
                                } catch (NoSuchAlgorithmException e) {
                                    e.printStackTrace();
                                } catch (IOException ioException) {
                                    ioException.printStackTrace();
                                } catch (SQLException throwables) {
                                    throwables.printStackTrace();
                                } catch (ClassNotFoundException classNotFoundException) {
                                    classNotFoundException.printStackTrace();
                                }
                                m.setVisible(true);
                            }
                        });
                        dispose();
                    }
                    else {
                        JOptionPane.showMessageDialog(loginButton, "The login credentials are wrong! Please try again");
                    }
                } catch (IOException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                }
            }
        });

        //button to register
        JButton registerButton = new JButton("register");
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(() -> {
                    RegisterGUI m = null;
                    try {
                        m = new RegisterGUI();

                    } finally {
                        m.setVisible(true);
                    }
                    dispose();
                });
            }
        });


        //Layout
        Container pane = getContentPane();
        getRootPane().setDefaultButton(loginButton);
        GridBagLayout gbl = new GridBagLayout();
        pane.setLayout(gbl);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5,5,5,5);

        UShareGUI.addComponentToPane(pane, gbl, gbc, usernameLabel, 0,0,1,1);
        UShareGUI.addComponentToPane(pane, gbl, gbc, usernameField, 1,0,3,1);
        UShareGUI.addComponentToPane(pane, gbl, gbc, passwordLabel, 0,1,1,1);
        UShareGUI.addComponentToPane(pane, gbl, gbc, passwordField, 1,1,3,1);
        UShareGUI.addComponentToPane(pane, gbl, gbc, loginButton, 1,2,1,1);
        UShareGUI.addComponentToPane(pane, gbl, gbc, registerButton, 2,2,1,1);

        pack();
    }

    public static void main(String[] args) throws IOException, CryptoException {
        EventQueue.invokeLater(() -> {
            LoginGUI m = null;
            try {
                m = new LoginGUI();

            } finally {
                m.setVisible(true);
            }
        });
    }
}
