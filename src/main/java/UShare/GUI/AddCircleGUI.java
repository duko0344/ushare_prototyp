package UShare.GUI;

import UShare.Cryptography.RSAKeyPair;
import UShare.Cryptography.RSAKeyPairGenerator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class AddCircleGUI extends JFrame {

    public AddCircleGUI(List friendsList){
        //GUI
        setTitle("Meine GUI");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JTextField circleName = new JTextField();
        JTextField publicKey = new JTextField();
        JTextField privateKey = new JTextField();

        //List with all the members of the circle
        List memberList = new List();


        //Button to check the credentials and log into the main GUI
        JButton addFriendButton = new JButton("Add friend to circle");
        addFriendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean doubleEntry = false;
                if(friendsList.getSelectedItem() != null){
                    for(int i = 0; i < memberList.getItemCount(); i++){
                        if(memberList.getItem(i).equals(friendsList.getSelectedItem())){
                            doubleEntry = true;
                            break;
                        }
                    }
                    if(!doubleEntry){
                        memberList.add(friendsList.getSelectedItem());
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "A specific user can only be added to a circle once!", "Warning", JOptionPane.WARNING_MESSAGE);
                    }

                }
                else {
                    JOptionPane.showMessageDialog(null, "Neither Username nor public key can be left empty.\nThe username can also not contain a space.\n" +
                            "The public key is optional but necessary if you want to share a file with this person outside of a circle.", "Warning", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        //button to cancel the creation of a new circle
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        //button to actually create a circle
        JButton createCircleButton = new JButton("Create Circle");
        createCircleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    FileWriter myWriter = new FileWriter("circles.txt", true);
                    myWriter.write(circleName.getText() + " ");
                    for (int i = 0; i < memberList.getItemCount(); i++) {
                        myWriter.write(memberList.getItem(i) + " ");
                    }
                    myWriter.write(privateKey.getText() + " ");
                    myWriter.write(publicKey.getText());
                    myWriter.close();
                    UShareGUI.updateAllListsFromTxt();
                    dispose();
                } catch (FileNotFoundException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });

        JButton generateKeysButton = new JButton("Generate keys");
        generateKeysButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final RSAKeyPairGenerator generator = new RSAKeyPairGenerator();
                final RSAKeyPair keyPair = generator.generate();
                publicKey.setText(keyPair.getBase64PublicKey().replaceAll("\\s+",""));
                System.out.println(keyPair.getBase64PublicKey());
                privateKey.setText(keyPair.getBase64PrivateKey().replaceAll("\\s+",""));
                System.out.println(keyPair.getBase64PrivateKey());
            }
        });

        //Labels
        JLabel circleNameLabel = new JLabel("Put in the name of the circle");
        JLabel publicKeyLabel = new JLabel("Put in the public key of the circle");
        JLabel privateKeyLabel = new JLabel("Put in the private key of the circle");
        JLabel selectableFriendsLabel = new JLabel("List of selectable friends");
        JLabel circleMembersLabel = new JLabel("List of members in the circle");

        //Layout
        Container pane = getContentPane();
        GridBagLayout gbl = new GridBagLayout();
        pane.setLayout(gbl);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5,5,5,5);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(circleNameLabel, gbc);
        pane.add(circleNameLabel);

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridwidth = 2;
        gbc.gridheight = 1;
        gbl.setConstraints(circleName, gbc);
        pane.add(circleName);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(publicKeyLabel, gbc);
        pane.add(publicKeyLabel);

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        gbc.gridheight = 1;
        gbl.setConstraints(publicKey, gbc);
        pane.add(publicKey);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(privateKeyLabel, gbc);
        pane.add(privateKeyLabel);

        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(privateKey, gbc);
        pane.add(privateKey);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(selectableFriendsLabel, gbc);
        pane.add(selectableFriendsLabel);

        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        gbc.gridheight = 2;
        gbl.setConstraints(friendsList, gbc);
        pane.add(friendsList);

        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(circleMembersLabel, gbc);
        pane.add(circleMembersLabel);

        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        gbc.gridheight = 2;
        gbl.setConstraints(memberList, gbc);
        pane.add(memberList);

        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(addFriendButton, gbc);
        pane.add(addFriendButton);

        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(generateKeysButton, gbc);
        pane.add(generateKeysButton);

        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(createCircleButton, gbc);
        pane.add(createCircleButton);

        gbc.gridx = 1;
        gbc.gridy = 7;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbl.setConstraints(cancelButton, gbc);
        pane.add(cancelButton);
        pack();

    }
}
