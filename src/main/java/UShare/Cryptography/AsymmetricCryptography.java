package UShare.Cryptography;

import org.apache.commons.io.FileUtils;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.security.*;

public class AsymmetricCryptography {
    private Cipher cipher;

    /**
     * The constructor of the AsymmetricCryptography class, using the RSA-cryptosystem
     */
    public AsymmetricCryptography() throws NoSuchAlgorithmException, NoSuchPaddingException {
        this.cipher = Cipher.getInstance("RSA");
    }

    /**
     * A method decrypts a String and writes the resulting byte array to a file in the installation folder of the program.
     *
     * @param path             the encrypted String
     * @param publicKey        the public key used for encryption
     *
     * @return String           the encrypted data
     * @throws CryptoException  if the publicKey cannot decrypt the encrypted symmetric key, or the symmetric key cannot decrypt the data.
     * @throws IOException
     */
    public String encryptFile(String path, String publicKey) throws IOException, CryptoException {
        File initialFile = new File(path);
        AsymmetricCipher cipher = new AsymmetricCipher();
        if(initialFile.exists() && !initialFile.isDirectory()) {
            byte[] bytePicture = FileUtils.readFileToByteArray(initialFile);
            // Generate a random symmetric key, use it to encrypt the data,  encrypt the random key using the private key
            // and return a CryptoPacket containing the encrypted data and the encrypted random key.
            final CryptoPacket cryptoPacket = cipher.encrypt(bytePicture, publicKey);
            // Convert the CryptoPacket into a Base64 String that can be readily reconstituted at the other end.
            final CryptoPacketConverter cryptoPacketConverter = new CryptoPacketConverter();
            try {
                final String base64EncryptedData = cryptoPacketConverter.convert(cryptoPacket);
                System.out.println("The final String is " + base64EncryptedData);
                return base64EncryptedData;
                //the next two lines are for decryption
                //byte[] encryptedBytes = cipher.decrypt(cryptoPacketConverter.convert(base64EncryptedData), keyPair.getBase64PublicKey());
                //FileUtils.writeByteArrayToFile(new File("encrypted.jpg"),encryptedBytes );
            } catch (CryptoException e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("the file path " + initialFile.getAbsolutePath() + "is not valid");
        }
        return null;
    }

    /**
     * A method decrypts a String and writes the resulting byte array to a file in the installation folder of the program.
     *
     * @param encryptedData     the encrypted String
     * @param privateKey        the private key used for decryption
     * @param fileType          the type of the resulting file
     * @param fileHash          the hash of the soon to be decrypted file
     *
     * @return String           the file hash + file type used to be stored in a list and document
     * @throws CryptoException  if the publicKey cannot decrypt the encrypted symmetric key, or the symmetric key cannot decrypt the data.
     * @throws IOException
     */
    public String decryptFile(String encryptedData, String privateKey, String fileType, String fileHash) throws CryptoException, IOException {
        CryptoPacketConverter cryptoPacketConverter = new CryptoPacketConverter();
        AsymmetricCipher cipher = new AsymmetricCipher();
        byte[] encryptedBytes = cipher.decrypt(cryptoPacketConverter.convert(encryptedData), privateKey);
        FileUtils.writeByteArrayToFile(new File(fileHash + fileType), encryptedBytes);
        return fileHash + fileType;
    }
}