package UShare.Cryptography;

/**
 * Indicates there was a problem during encryption or decryption.
 *
 * @author william-ferguson-au GitHub: https://github.com/william-ferguson-au/asymmetric-crypto/tree/master/src/main/java
 */
public final class CryptoException extends Exception {

    /**
     * Creates a CryptoException with the desired output text.
     *
     * @param message message displayed when a crypto exception occurs
     */
    public CryptoException(String message) {
        super(message);
    }

    /**
     * Creates a CryptoException with the desired output text the cause of the error.
     *
     * @param message message displayed when a crypto exception occurs
     * @param cause   cause of the exception
     */
    public CryptoException(String message, Throwable cause) {
        super(message, cause);
    }
}
