package UShare.Cryptography;

/**
 * Represents the Base64 stringified values of an RSA Public Key Pair.
 *
 * @author william-ferguson-au GitHub: https://github.com/william-ferguson-au/asymmetric-crypto/tree/master/src/main/java
 */
public class RSAKeyPair {

    private final String privateKeyString;
    private final String publicKeyString;

    public RSAKeyPair(String privateKeyString, String publicKeyString) {
        this.privateKeyString = privateKeyString;
        this.publicKeyString = publicKeyString;
    }

    public String getBase64PrivateKey() {
        return privateKeyString;
    }

    public String getBase64PublicKey() {
        return publicKeyString;
    }
}
